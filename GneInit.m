//
//  GneInit.m
//  GLibNetworkingExt
//
//  Created by Dan on 20.09.2021.
//

#import "GneInit.h"

@implementation GneInit

+ (void)initialize {
    (void)NseInit.class;
    (void)TlsInit.class;
    
    gne_init();
}

@end
