//
//  GLibNetworkingExt.h
//  Pods
//
//  Created by Dan Kalinin on 12/30/20.
//

#import <GLibNetworkingExt/GneMain.h>
#import <GLibNetworkingExt/GneInit.h>

FOUNDATION_EXPORT double GLibNetworkingExtVersionNumber;
FOUNDATION_EXPORT const unsigned char GLibNetworkingExtVersionString[];
